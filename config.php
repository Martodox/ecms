<?php

$EnvironmentSettings = array(
    'berry-msi' => array(
        'host' => 'localhost',
        'username' => 'root',
        'password' => 'root',
        'database' => 'ecms',
        'devmode' => true,
        'defaultLanguage' => 'pl',
        'smartyCaching' => 0
    )
);

define("serviceName", "Even-Art Studio");
define('SMARTY_RESOURCE_CHAR_SET', 'UTF-8');
