<?php

/**
 * Description of EnvironmentSettings
 *
 * @author Bartosz Jakubowiak
 * @link http://bartosz.jakubowiak.pl/ Authors home page
 */
$settings = $EnvironmentSettings[gethostname()];

if (!isset($settings['rootpatch'])) {
    $settings['rootpatch'] = '/';
}
if (!isset($settings['devmode'])) {
    $settings['devmode'] = false;
}
if (!isset($settings['secureLogin'])) {
    $settings['secureLogin'] = true;
}
if (!isset($settings['loginTimeout'])) {
    $settings['loginTimeout'] = 5;
}
if (!isset($settings['defaultLanguage'])) {
    $settings['defaultLanguage'] = 'en';
}
if (!isset($settings['smartyCaching'])) {
    $settings['smartyCaching'] = 1;
}

define("config_MySql_host", $settings['host']);
define("config_MySql_username", $settings['username']);
define("config_MySql_password", $settings['password']);
define("config_MySql_dbname", $settings['database']);
define("devmode", $settings['devmode']);
define("rootpatch", $settings['rootpatch']);
define("secureLogin", $settings['secureLogin']);
define("loginTimeout", (int) $settings['loginTimeout']);
define("defaultLanguage", $settings['defaultLanguage']);
define("smartyCaching", (int) $settings['smartyCaching']);
